module.exports = {
  configDB: {
    user: "postgres",
    host: "postgres_db", // refer to docker-compose.yml file
    database: "example-node-postgres",
    password: "123456",
    port: 5432
  }
};
